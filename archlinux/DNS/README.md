DNS files
----------

- /etc/named.conf
- /var/named/127.0.0.zone
- /var/named/empty.zone
- /var/named/localhost.ip6.zone
- /var/named/localhost.zone
- /var/named/root.hint                 <--- ROOT SERVERS

---------

- /var/named/matrix.zone               <---   A Records 
- /var/named/10.1.zone                 <--- PTR Records (reverse)

